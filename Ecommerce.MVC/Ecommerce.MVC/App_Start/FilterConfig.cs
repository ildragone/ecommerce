﻿using Ecommerce.MVC.Handlers;
using System.Web;
using System.Web.Mvc;

namespace Ecommerce.MVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //Vado a aggiungere il mio AuthorizeAttribute personalizzato che sostituisce quello di default
            filters.Add(new AutorizzazioneMvc());
        }
    }
}
