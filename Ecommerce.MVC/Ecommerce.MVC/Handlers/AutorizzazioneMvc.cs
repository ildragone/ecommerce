﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Ecommerce.MVC.Handlers
{
    //Mi serve per personalizzare il comportamente dell'authorize di mvc dato che FormsIdentity non supporta il ruoli
    //Questa classe va aggiunta ai filtri di FilterConfig.cs per renderla operativa
    public class AutorizzazioneMvc : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            FormsIdentity identity = filterContext.HttpContext.User.Identity as FormsIdentity;
            if (identity != null && identity.Ticket != null)
            {
                //Sostituisci la FormsIdentity con una GenericIdentity in cui posso impostare il ruoli che mi prendo dal ticket del cookie
                filterContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(identity.Name), identity.Ticket.UserData.Split(':'));
            }
        }
    }
}