﻿using Ecommerce.Core;
using Ecommerce.Core.DTO;
using Ecommerce.Core.Utils;
using Ecommerce.MVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Ecommerce.MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel viewModel)
        {
            using (var s = new Services())
            {
                Result<ClienteDTO> result = s.Login(viewModel.Username, viewModel.Password);
                if (!result.Successo)
                    ModelState.AddModelError(string.Empty, result.Errore);
                if (!ModelState.IsValid)
                {
                    viewModel.Password = null;
                    return View(viewModel);
                }
                //Aggiungo il ruolo di cliente come predefinito
                List<string> ruoli = new List<string>() { "cliente" };
                //Aggiungo il ruolo di admin se ho questo username. Per mancanza di tempo è stato fatto qui, andrebbe fatto nei servizi
                if (result.Data.Username == "michele.bravi@e-lios.eu")
                    ruoli.Add("admin");
                //Creo l'oggetto ticket che contiene la mia identità, come ultimo parametro gli passo i ruoli.
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, result.Data.Username, DateTime.Now,
                    DateTime.Now.AddHours(1), false, string.Join(":", ruoli));
                //aggiungo questo ticket in un cookie
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
                cookie.Expires = DateTime.Now.AddHours(1);
                //Allego il cookie alla risposta http che verà inviata quando viene eseguito il RedirectToAction
                Response.Cookies.Add(cookie);
                return RedirectToAction(nameof(HomeController.Index));
            }
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction(nameof(HomeController.Index));
        }
    }
}