﻿using Ecommerce.Core;
using Ecommerce.MVC.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Ecommerce.MVC.Controllers
{
    //Permetto l'esecuzione di tutte le action di questo controller solo se ho il ruolo di amministratore
    [Authorize(Roles = "admin")]
    public class ArticoliController : Controller
    {
        public ActionResult Index()
        {
            using (var s = new Services())
            {
                var risultato = s.GetArticoli();
                if (!risultato.Successo)
                {
                    ViewBag.Errore = risultato.Errore;
                    return View();
                }

                return View(new ArticoliViewModel
                {
                    Articoli = risultato.Data
                });
            }
        }

        [HttpGet] // opzionale
        public ActionResult Inserisci()
        {
            return View(new ArticoloViewModel
            {
                IsNuovo = true // Indica alla view che stiamo effettuando un nuovo inserimento
            });
        }

        [HttpPost]
        public ActionResult Inserisci(ArticoloViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            string percorsoImmagine = null;
            if (model.Immagine != null)
            {
                // Se un'immagine è stata postata sul server, la salviamo e otteniamo il percorso in cui è salvata
                var cartellaFisica = Server.MapPath("~/App_Data/ImmaginiArticoli");
                var estensione = Path.GetExtension(model.Immagine.FileName);
                // Per evitare immagini con lo stesso nome, gli riassegnamo un nome univoco con Guid, mantenendo l'estensione di com'è arrivata
                var percorsoFile = Path.Combine(cartellaFisica, $"{Guid.NewGuid()}{estensione}");

                using (var stream = model.Immagine.InputStream)
                using (var fileStream = System.IO.File.Open(percorsoFile, FileMode.CreateNew))
                {
                    stream.CopyTo(fileStream);
                }

                percorsoImmagine = percorsoFile;
            }

            using (var s = new Services())
            {
                var result = s.InserisciArticolo(model.Codice, model.Nome, model.Descrizione,
                     decimal.Parse(model.Prezzo), percorsoImmagine);
                if (!result.Successo)
                {
                    ViewBag.Errore = result.Errore;
                    return View(model);
                }
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet] // opzionale
        public ActionResult Modifica(string codice)
        {
            using (var s = new Services())
            {
                var risultato = s.GetArticolo(codice);
                if (!risultato.Successo)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);

                var articolo = risultato.Data;

                // Siccome siamo nella action "Modifica" ma vogliamo usare la view "Inserisci", dobbiamo specificarlo
                return View("Inserisci", new ArticoloViewModel
                {
                    IsNuovo = false, // Indica alla view che stiamo effettuando una modifica di un elemento esistente
                    Codice = articolo.Codice,
                    Descrizione = articolo.Descrizione,
                    Nome = articolo.Nome,
                    Prezzo = articolo.Prezzo.ToString()
                });
            }
        }

        [HttpPost]
        public ActionResult Modifica(string codice, ArticoloViewModel model)
        {
            if (!ModelState.IsValid)
                // Siccome siamo nella action "Modifica" ma vogliamo usare la view "Inserisci", dobbiamo specificarlo
                return View("Inserisci", model);

            string percorsoImmagine = null;
            if (model.Immagine != null)
            {
                // Se un'immagine è stata postata sul server, la salviamo e otteniamo il percorso in cui è salvata
                var cartellaFisica = Server.MapPath("~/App_Data/ImmaginiArticoli");
                var estensione = Path.GetExtension(model.Immagine.FileName);
                // Per evitare immagini con lo stesso nome, gli riassegnamo un nome univoco con Guid, mantenendo l'estensione di com'è arrivata
                var percorsoFile = Path.Combine(cartellaFisica, $"{Guid.NewGuid()}{estensione}");

                using (var stream = model.Immagine.InputStream)
                using (var fileStream = System.IO.File.Open(percorsoFile, FileMode.CreateNew))
                {
                    stream.CopyTo(fileStream);
                }

                percorsoImmagine = percorsoFile;
            }

            using (var s = new Services())
            {
                var result = s.ModificaArticolo(codice, model.Nome, model.Descrizione, decimal.Parse(model.Prezzo), percorsoImmagine);
                if (!result.Successo)
                {
                    ViewBag.Errore = result.Errore;
                    // Siccome siamo nella action "Modifica" ma vogliamo usare la view "Inserisci", dobbiamo specificarlo
                    return View("Inserisci", model);
                }
            }

            return RedirectToAction(nameof(Index));
        }

        public ActionResult Immagine(string codice)
        {
            using (var s = new Services())
            {
                var risultato = s.GetArticolo(codice);
                if (!risultato.Successo)
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);

                var articolo = risultato.Data;

                if (string.IsNullOrEmpty(articolo.PercorsoImmagine))
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                // Leggi i bytes dell'immagine salvata sul server
                var bytesImmagine = System.IO.File.ReadAllBytes(articolo.PercorsoImmagine);

                // Restituisci l'immagine al client, con il content/type corrispondente all'estensione dell'immagine
                return File(bytesImmagine, MimeMapping.GetMimeMapping(articolo.PercorsoImmagine));
            }
        }
    }
}
