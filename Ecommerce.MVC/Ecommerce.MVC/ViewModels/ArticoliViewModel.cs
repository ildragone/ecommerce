﻿using Ecommerce.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ecommerce.MVC.ViewModels
{
    public class ArticoliViewModel
    {
        public IEnumerable<ArticoloDTO> Articoli { get; set; }
    }
}
