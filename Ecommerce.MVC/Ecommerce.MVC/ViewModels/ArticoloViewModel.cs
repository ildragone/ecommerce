﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ecommerce.MVC.ViewModels
{
    public class ArticoloViewModel
    {
        public bool IsNuovo { get; set; }

        [Required]
        [MaxLength(16, ErrorMessage = "Il campo {0} deve avere al max. lunghezza {1}")]
        public string Codice { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string Descrizione { get; set; }

        [Required]
        public string Prezzo { get; set; }

        public HttpPostedFileBase Immagine { get; set; }
    }
}
