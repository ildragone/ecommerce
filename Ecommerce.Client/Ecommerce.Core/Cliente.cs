﻿using Ecommerce.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Core
{
    public class Cliente
    {
        public int ID { get; private set; }

        private string username;
        public string Username
        {
            get { return username; }
            private set { username = value; }
        }

        public string Password { get; private set; }

        public string Nome { get; private set; }
        public string Cognome { get; private set; }
        public string Email { get; private set; }
        public string CodiceFiscale { get; private set; }

        public DateTime DataRegistrazione { get; private set; }

        public virtual ICollection<Indirizzo> Indirizzi { get; private set; }

        protected Cliente() { }

        private Cliente(string username, string password, string nome,
            string cognome, string email, DateTime dataRegistrazione,
            string codiceFiscale, IEnumerable<Indirizzo> indirizzi)
        {
            this.Username = username;
            this.Password = password;
            this.Nome = nome;
            this.Cognome = cognome;
            this.Email = email;
            this.DataRegistrazione = dataRegistrazione;
            this.CodiceFiscale = codiceFiscale;
            this.Indirizzi = indirizzi.ToList();
        }

        public static Result<Cliente> CreaCliente(string username, string password,
            string nome, string cognome, string email, DateTime dataRegistrazione,
            string codiceFiscale, IEnumerable<Indirizzo> indirizzi)
        {
            if (password.Length < 8)
                return Result<Cliente>.CreaErrore("La password dev'essere lunga almeno 8 caratteri.");

            if (!email.IsEmailValida())
                return Result<Cliente>.CreaErrore("Email non valida.");

            var cliente = new Cliente(username, password, nome, cognome, email, dataRegistrazione,
             codiceFiscale, indirizzi);

            return Result<Cliente>.CreaSuccesso(cliente);
        }

        public Result<object> ModificaEmail(string email)
        {
            if (!email.IsEmailValida())
                return Result<object>.CreaErrore("Email non valida.");

            this.Email = email;
            return Result<object>.CreaSuccesso(null);
        }

        public void ModificaAnagrafica(string nome, string cognome,
            string codiceFiscale)
        {
            this.Nome = nome;
            this.Cognome = cognome;
            this.CodiceFiscale = codiceFiscale;
        }

        public string Descrizione
        {
            get
            {
                return Nome + " " + Cognome + " (" + Email + ")";
            }
        }
    }
}
