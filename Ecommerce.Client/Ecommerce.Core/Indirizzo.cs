﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Core
{
    public class Indirizzo
    {
        public int ID { get; private set; }

        public string Via { get; private set; }
        public string Città { get; private set; }
        public string CAP { get; private set; }

        public int ClienteID { get; private set; }
        public virtual Cliente Cliente { get; private set; }

        public Indirizzo(string via, string città, string cap)
        {
            this.Via = via;
            this.Città = città;
            this.CAP = cap;
        }
    }
}
