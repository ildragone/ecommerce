﻿using Ecommerce.Core.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Core
{
    [Table("Articoli")]
    public class Articolo
    {
        [MaxLength(16)]
        [Required] // Siccome Codice è chiave primaria, è forzatamente Required
        public string Codice { get; private set; }

        public string Nome { get; private set; }
        public string Descrizione { get; private set; }

        public decimal? Prezzo { get; private set; } // decimal? è equivalente a Nullable<decimal>
        public string PercorsoImmagine { get; private set; }

        // Un costruttore senza parametri è necessario per Entity Framework per costruire la classe dai dati del DB
        protected Articolo() { }

        private Articolo(string codice, string nome, string descrizione, decimal prezzo, string percorsoImmagine)
        {
            this.Codice = codice;
            this.Nome = nome;
            this.Descrizione = descrizione;
            this.Prezzo = prezzo;
            this.PercorsoImmagine = percorsoImmagine;
        }

        public static Result<Articolo> CreaArticolo(string codice, string nome, string descrizione,
            decimal prezzo, string percorsoImmagine)
        {
            if (prezzo <= 0)
                return Result<Articolo>.CreaErrore("Il prezzo non è valido.");

            //if (!File.Exists(percorsoImmagine))
            //    return Result<Articolo>.CreaErrore(string.Format("Il file {0} non esiste.", percorsoImmagine));

            var articolo = new Articolo(codice, nome, descrizione, prezzo, percorsoImmagine);
            return Result<Articolo>.CreaSuccesso(articolo);
        }

        public void Modifica(string nome, string descrizione, decimal prezzo, string percorsoImmagine)
        {
            this.Nome = nome;
            this.Descrizione = descrizione;
            this.Prezzo = prezzo;

            // Se un nuovo percorso immagine è specificato, modificalo, altrimenti mantieni il precedente
            if (!string.IsNullOrEmpty(percorsoImmagine))
                this.PercorsoImmagine = percorsoImmagine;
        }
    }
}
// "Il file " + percorsoImmagine + " non esiste."
// string.Concat("Il file ", percorsoImmagine, " non esiste.")
// string.Format("Il file {0} non esiste.", percorsoImmagine)
// $"Il file {percorsoImmagine} non esiste."