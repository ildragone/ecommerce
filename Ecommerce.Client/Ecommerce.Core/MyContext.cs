﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Core
{
    public class MyContext : DbContext
    {
        // FIX per far funzionare EntityFramework sui progetti che referenziano Ecommerce.Core
        static MyContext()
        {
            SqlProviderServices dependencySqlServerDll = SqlProviderServices.Instance;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<MyContext>(null);
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Articolo>().ToTable("Articoli").HasKey(a => a.Codice);
            modelBuilder.Entity<Articolo>().Property(a => a.Nome).HasMaxLength(100);

            modelBuilder.Entity<Cliente>().ToTable("Clienti").HasKey(c => c.ID);
            modelBuilder.Entity<Cliente>().Property(c => c.Username).HasMaxLength(50);

            modelBuilder.Entity<Indirizzo>().ToTable("Indirizzi").HasKey(i => i.ID);
            modelBuilder.Entity<Indirizzo>().HasRequired(i => i.Cliente).WithMany(c => c.Indirizzi).HasForeignKey(i => i.ClienteID);
        }

        public DbSet<Articolo> Articoli { get { return Set<Articolo>(); } }
        public DbSet<Cliente> Clienti { get { return Set<Cliente>(); } }
    }
}
