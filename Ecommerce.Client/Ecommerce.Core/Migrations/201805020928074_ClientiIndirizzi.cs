namespace Ecommerce.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientiIndirizzi : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clienti",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Nome = c.String(),
                        Cognome = c.String(),
                        Email = c.String(),
                        CodiceFiscale = c.String(),
                        DataRegistrazione = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Indirizzi",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Via = c.String(),
                        Città = c.String(),
                        CAP = c.String(),
                        ClienteID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Clienti", t => t.ClienteID, cascadeDelete: true)
                .Index(t => t.ClienteID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Indirizzi", "ClienteID", "dbo.Clienti");
            DropIndex("dbo.Indirizzi", new[] { "ClienteID" });
            DropTable("dbo.Indirizzi");
            DropTable("dbo.Clienti");
        }
    }
}
