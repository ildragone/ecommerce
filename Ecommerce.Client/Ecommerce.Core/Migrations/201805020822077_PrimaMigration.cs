namespace Ecommerce.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrimaMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articoli",
                c => new
                    {
                        Codice = c.String(nullable: false, maxLength: 16),
                        Nome = c.String(maxLength: 100),
                        Descrizione = c.String(),
                        Prezzo = c.Decimal(precision: 18, scale: 2),
                        PercorsoImmagine = c.String(),
                    })
                .PrimaryKey(t => t.Codice);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Articoli");
        }
    }
}
