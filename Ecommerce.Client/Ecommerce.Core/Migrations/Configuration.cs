namespace Ecommerce.Core.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Ecommerce.Core.MyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;

            //AutomaticMigrationsEnabled = true;
            //AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Ecommerce.Core.MyContext context)
        {
            if (!context.Articoli.Any())
            {
                var risultato = Articolo.CreaArticolo("COD01", "Dell Inspiron", "Portatile da gaming", 1000, null);
                if (!risultato.Successo)
                    throw new Exception(risultato.Errore);

                context.Articoli.Add(risultato.Data);

            }

            // Non � necessario perch� in questo caso lo fa al termine del Seed
            //context.SaveChanges();
        }
    }
}
