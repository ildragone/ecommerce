﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Core.Utils
{
    public class Result<T>
    {
        public bool Successo { get; private set; }
        public string Errore { get; private set; }
        public T Data { get; private set; }

        private Result(bool successo, string errore, T data)
        {
            this.Successo = successo;
            this.Errore = errore;
            this.Data = data;
        }

        public static Result<T> CreaErrore(string errore)
        {
            return new Result<T>(false, errore, default(T));
        }

        public static Result<T> CreaSuccesso(T data)
        {
            return new Result<T>(true, null, data);
        }
    }
}
