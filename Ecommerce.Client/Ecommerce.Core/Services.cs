﻿using Ecommerce.Core.DTO;
using Ecommerce.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Core
{
    public class Services : IDisposable
    {
        private MyContext context;

        public Services()
        {
            context = new MyContext();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public Result<IEnumerable<ArticoloDTO>> GetArticoli(string ricerca = null)
        {
            try
            {
                // Componiamo la query
                var query = context.Articoli.Select(a => new ArticoloDTO
                {
                    Codice = a.Codice,
                    Nome = a.Nome
                });

                if (!string.IsNullOrEmpty(ricerca))
                    query = query.Where(a => a.Nome.Contains(ricerca));

                // La query viene effettivamente eseguita sul DB
                var risultato = query.ToList();

                return Result<IEnumerable<ArticoloDTO>>.CreaSuccesso(risultato);
            }
            catch (Exception ex)
            {
                // TODO: LOG dettaglio errore per sviluppatori! ex.Message

                // Ritorna all'utente un messaggio di errore generico
                return Result<IEnumerable<ArticoloDTO>>.CreaErrore("Si è verificato un errore sul server.");
            }
        }

        public Result<Articolo> GetArticolo(string codice)
        {
            try
            {
                var articolo = context.Articoli.Find(codice);
                if (articolo == null)
                    return Result<Articolo>.CreaErrore($"L'articolo con codice {codice} non è stato trovato.");

                return Result<Articolo>.CreaSuccesso(articolo);
            }
            catch (Exception ex)
            {
                // TODO: LOG dettaglio errore per sviluppatori! ex.Message

                // Ritorna all'utente un messaggio di errore generico
                return Result<Articolo>.CreaErrore("Si è verificato un errore sul server.");
            }
        }

        public Result<Articolo> InserisciArticolo(string codice, string nome, string descrizione, decimal prezzo, string percorsoImmagine)
        {
            try
            {
                var esisteGià = context.Articoli.Any(a => a.Codice == codice);
                if (esisteGià)
                    return Result<Articolo>.CreaErrore("Esiste già un articolo con lo stesso codice.");

                var risultato = Articolo.CreaArticolo(codice, nome, descrizione, prezzo, percorsoImmagine);
                if (!risultato.Successo)
                    return Result<Articolo>.CreaErrore(risultato.Errore);

                var nuovoArticolo = risultato.Data;

                context.Articoli.Add(nuovoArticolo);
                context.SaveChanges();

                return Result<Articolo>.CreaSuccesso(nuovoArticolo);
            }
            catch (Exception ex)
            {
                // TODO: LOG dettaglio errore per sviluppatori! ex.Message

                // Ritorna all'utente un messaggio di errore generico
                return Result<Articolo>.CreaErrore("Si è verificato un errore sul server.");
            }
        }

        public Result<object> ModificaArticolo(string codice, string nome, string descrizione, decimal prezzo, string percorsoImmagine)
        {
            try
            {
                var articolo = context.Articoli.Find(codice);
                if (articolo == null)
                    return Result<object>.CreaErrore($"L'articolo con codice {codice} non esiste.");

                articolo.Modifica(nome, descrizione, prezzo, percorsoImmagine);
                context.SaveChanges();

                return Result<object>.CreaSuccesso(null);
            }
            catch (Exception ex)
            {
                // TODO: LOG dettaglio errore per sviluppatori! ex.Message

                // Ritorna all'utente un messaggio di errore generico
                return Result<object>.CreaErrore("Si è verificato un errore sul server.");
            }
        }

        public Result<ClienteDTO> Login(string username, string password)
        {
            Cliente cliente = context.Clienti.SingleOrDefault(c => c.Username == username && c.Password == password);
            if (cliente == null)
                return Result<ClienteDTO>.CreaErrore("Username o password non validi");
            ClienteDTO clienteDTO = new ClienteDTO()
            {
                Username = cliente.Username,
                Nome = cliente.Nome,
                Cognome = cliente.Cognome,
                Email = cliente.Email,
                CodiceFiscale = cliente.CodiceFiscale
            };
            return Result<ClienteDTO>.CreaSuccesso(clienteDTO);
        }
    }
}
