﻿using Ecommerce.Core;
using Ecommerce.Core.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            EsempioOttenimentoArticoli();
            EsempioInserimento();


            using (var c = new MyContext())
            {
                string password = Console.ReadLine();

                var arrayIndirizzi = new Indirizzo[]
                {
                    new Indirizzo("via Verdi 35", "Macerata", "62100"),
                    new Indirizzo("via Verdi 1", "Camerino", "62032")
                };

                var risultato = Cliente.CreaCliente("User1", password, "Mario", "Rossi",
                    "alexglabs@gmail.com", DateTime.Now, "ABC123.....", arrayIndirizzi);

                if (risultato.Successo)
                {
                    c.Clienti.Add(risultato.Data);
                    c.SaveChanges();
                    Console.WriteLine("Cliente creato correttamente.");
                }
                else
                    Console.WriteLine(risultato.Errore);
            }

            /*
            var articolo = Articolo.CreaArticolo("TVL0001", "Tavolo", "Tavolo in legno massello",
                499.99m, @"C:\Udfdfsers\Alessio\Desktop\tavolo-per-sala-da-pranzo-in-massello-di-legno-di-sheesham-l-140-cm-1000-5-17-129987_1.jpg");



            var x = new List<object>();

            var indirizzi = new List<Indirizzo>()
                {
                    new Indirizzo("via Verdi 35", "Macerata", "62100"),
                    new Indirizzo("via Verdi 1", "Camerino", "62032")
                };
                */
            //indirizzi.Add(new Indirizzo("via Verdi 35", "Macerata", "62100"));
            //indirizzi.Add(new Indirizzo("via Verdi 1", "Camerino", "62032"));



            //c.ModificaAnagrafica("Luca", "Esposito", "CF1234567");

        }

        static void EsempioInserimento()
        {
            using (var s = new Services())
            {
                Console.WriteLine("Inserisci codice: ");
                var codice = Console.ReadLine();
                Console.WriteLine("Inserisci nome: ");
                var nome = Console.ReadLine();
                Console.WriteLine("Inserisci descrizione: ");
                var desc = Console.ReadLine();
                Console.WriteLine("Inserisci prezzo: ");
                var prezzo = Console.ReadLine();

                var risultato = s.InserisciArticolo(codice, nome, desc, decimal.Parse(prezzo), null);
                if (!risultato.Successo)
                    Console.WriteLine(risultato.Errore);
                else
                    Console.WriteLine("Articolo inserito correttamente!");

                Console.ReadLine();
            }
        }

        static void EsempioOttenimentoArticoli()
        {
            using (var s = new Services())
            {
                Console.WriteLine("Cerca: ");
                var ricerca = Console.ReadLine();

                var esitoTuttiGliArticoli = s.GetArticoli(ricerca);
                if (!esitoTuttiGliArticoli.Successo)
                {
                    Console.WriteLine(esitoTuttiGliArticoli.Errore);
                    return;
                }

                foreach (var articolo in esitoTuttiGliArticoli.Data)
                {
                    Console.WriteLine($"{articolo.Codice}: {articolo.Nome}");
                }
            }

            Console.ReadLine();
        }

        static void EsempiLINQ()
        {
            // Con due parametri in input
            Func<int, string, string> f = (x1, x2) =>
            {
                return x1.ToString() + x2;
            };

            // Con un solo parametro int x1
            Func<int, string> f2 = x1 => x1.ToString();

            // Procedura inline (nessun return)
            Action<int> a = (n) =>
            {
                Console.WriteLine(n);
            };

            a(3);
            a.Invoke(3);

            var ris = f(123, "string");


            var clienti = new List<Cliente>()
            {
                Cliente.CreaCliente("mario.rossi", "12345678", "Mario", "Rossi",
                    "mario.rossi@email.it", DateTime.Now, "MRRSS123",
                    Enumerable.Empty<Indirizzo>()).Data,

                Cliente.CreaCliente("luca.bianco", "12345678", "Luca", "Bianco",
                    "luca.bianco@email.it", DateTime.Now, "LCBMRG12",
                    Enumerable.Empty<Indirizzo>()).Data,

                Cliente.CreaCliente("francesca.rossi", "12345678", "Francesca", "Rossi",
                    "francesca.rossi@email.it", DateTime.Now, "FRNRSS1434",
                    Enumerable.Empty<Indirizzo>()).Data,
            };

            var clientiBrevi = clienti.Select(c => new
            {
                NomeBreve = c.Nome,
                CognomeBreve = c.Cognome,
                Email = new MailAddress(c.Email),
                Denominazione = c.Nome + " " + c.Cognome
            });

            var gruppi = clienti.GroupBy(c => c.Cognome);
            var gruppoRossi = gruppi.Single(g => g.Key == "Rossi");

            // Notazione Query Expression
            var qExpr = from c in clienti
                        where c.Cognome == "Rossi"
                        select c;

            // Notazione .NET Notation
            var dotnetnot = clienti.Where(c => c.Cognome == "Rossi");
        }

        static void EsempioUsing()
        {
            // CON Using
            {
                try
                {
                    using (FileStream f = File.OpenRead(@"C:\Users\Alessio\Desktop\Progetto Programmazione C#.pdf"))
                    using (BinaryReader br = new BinaryReader(f))
                    {
                        throw new Exception();
                    }
                }
                catch (Exception)
                {
                    // L'eccezione viene gestita ma la fine dello using } viene comunque eseguita
                }
            }

            // Equivalente senza Using
            {
                try
                {
                    FileStream f = File.OpenRead(@"C:\Users\Alessio\Desktop\Progetto Programmazione C#.pdf");
                    BinaryReader br = new BinaryReader(f);

                    throw new Exception();

                    br.Dispose();
                    f.Dispose();
                }
                catch (Exception)
                {
                    // L'eccezione viene gestita saltando i Dispose()
                }
            }
        }
    }
}
